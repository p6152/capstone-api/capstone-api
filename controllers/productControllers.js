const Product = require(`./../models/Products`)

// Retrieve all active products
module.exports.activeProducts = async () => {
	return await Product.find({isActive: true}).then(result => result)
}

// Create Products
module.exports.addProduct = async (reqBody) => {
	
	const {productName, description, price, qty} = reqBody

	return await Product.findOne({productName: reqBody.productName}).then(result => {
		if(result != null && result.productName == reqBody.productName){
			return false
		}else{
			if(result == null){
				const newProduct = new Product({
					productName: productName,
					description: description, 
					price: price,
					qty: qty
				})

				return newProduct.save().then(result => {
					if(result){
						return true
					}else {
						if(result == null){
							return false
						}
					}
				})
			}	
		}	
	})		
}


// Retrieve all products both active and not
module.exports.showProducts = async () => {

	return await Product.find().then(result => result)
}

//Retrieve Single Active Product
module.exports.getProduct = async (id) => {

	return await Product.findById(id).then( product => {
		if(product == null){
			return `No existing product`

		} else if(product.isActive == false){
			return `This product is out of stock`
		}
		else {
			if(product !== null){
				return product
			}
		}
	})
}

//Retrieve Single Active Product
module.exports.getOneProduct = async (id) => {

	return await Product.findById(id).then( product => {
		if(product == null){
			return `No existing product`
		}
		else {	
			return product
		}
	})
}



// Update Product
module.exports.updateProduct = async (productId, reqBody) => {
	return await Product.findByIdAndUpdate(productId, {$set: reqBody}, {new: true}).then(result => result)
}

// ARCHIVE PRODUCT
module.exports.archiveProduct = async (productId, reqBody) => {
	const {isActive} = reqBody
	return await Product.findByIdAndUpdate(productId, {$set: {isActive: false}}, {new: true}).then(result => result)
}

// UNARCHIVE PRODUCT
module.exports.unarchiveProduct = async (productId, reqBody) => {
	const {isActive} = reqBody
	return await Product.findByIdAndUpdate(productId, {$set: {isActive: true}}, {new: true}).then(result => result)
}

// DELETE A PRODUCT FROM THE DB
module.exports.deleteProduct = async (productId) => {
	return await Product.findOneAndDelete({_id: productId}).then((result, err) => result ? true : err)
}




