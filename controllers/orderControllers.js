const Order = require(`./../models/Orders`)
const Product = require(`./../models/Products`)

// CREATE AN ORDER
module.exports.createOrder = async (data) => {
	
	const {userId, productId, price, qty} = data

	return await Product.findOne({_id: productId}).then(result => {
		//console.log(qty)
		
		if(result != null){

				if (result.qty >= qty){

					let updatedProductQty = result.qty - qty
					let productTotal = price * qty
					subtotal = productTotal

					Product.findByIdAndUpdate(productId, {$set: {qty: updatedProductQty}}, {new: true}).then(result => result)

					const newOrder = new Order({
							userId: userId,
							productId: productId, 
							price: price,
							qty: qty,
							total: productTotal,
							subTotal: subtotal
						})

						newOrder.orderedProducts.push({productId: productId, price: price, qty:qty, total: productTotal})
						return newOrder.save().then(result => {
							if(result){
								return true
							}else {
								if(result == null){
									return false
								}
							}
						})

				}else{
					return `Product is out of stock. Available: ${result.qty}`
				}

		}else{
			return `Product Not found`
		}
	})		
}

// RETRIEVE ALL ORDERS
module.exports.showAllOrders = async () => {

	return await Order.find().then(result => result)
}

// RETRIEVE USER"S ORDER/S
module.exports.myOrders = async (userID) => {
	const id = userID.userId
	
	const userOrder = await Order.find({userId: id}).then(result => {
		return result
	})
	return userOrder
}

// DELETE AN ORDER FROM THE DB
module.exports.deleteOrder = async (reqBody) => {
	const {id} = reqBody
	return await Order.findOneAndDelete({_id: id}).then((result, err) => result ? true : err)
}

// UPDATE Order status to "Completed" 
module.exports.orderCompleted = async (orderId, reqBody) => {
	const {status, dateCompleted} = reqBody
	return await Order.findByIdAndUpdate(orderId, {$set: {status: "Completed", dateCompleted: new Date()},}, {new: true}).then(result => result)
}

