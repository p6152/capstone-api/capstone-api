const CryptoJS = require("crypto-js");
const User = require('./../models/Users')
const {createToken} = require('./../auth');

//REGISTER A CUSTOMER
module.exports.register = async (reqBody) => {
	
	const {firstName, lastName, email, password} = reqBody

	return await User.findOne({email: reqBody.email}).then(result => {
		if(result != null && result.email == reqBody.email){
			return `Email is already registered.`
		}else{
			if(result == null){
				const newUser = new User({
					firstName: firstName,
					lastName: lastName, 
					email: email,
					password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
				})

				return newUser.save().then(result => {
					if(result){
						return true
					}else {
						if(result == null){
							return false
						}
					}
				})
			}	
		}	
	})		
}

//GET ALL CUSTOMERS
module.exports.getAllCustomers = async () => {

	return await User.find().then(result => result)
}

// LOGIN A USER
module.exports.login = async (reqBody) => {

	return await User.findOne({email: reqBody.email}).then((result, err) => {

		if(result == null){
			return {message: `User does not exist.`}

		} else {

			if(result !== null){
				//check if pw is correct, decrypt to compare pw input from the user from pw stored in db
				const decryptedPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)

				console.log(reqBody.password == decryptedPw) //true
				

				if(reqBody.password == decryptedPw){
					//create a token for the user
					return { token: createToken(result) }
				} else {
					return false
				}

			} else {
				return err
			}
		}
	})
}

//RETRIEVE CUSTOMER INFORMATION
module.exports.profile = async (id) => {

	return await User.findById(id).then((result, err) => {
		if(result){
			return result
		}else{
			if(result == null){
				return {message: `user does not exist`}
			} else {
				return err
			}
		}
	})
}

module.exports.updateInfo = async (id, reqBody) => {
	const { firstName, lastName, email } = reqBody
	return await User.findByIdAndUpdate(id, {$set: {firstName: firstName, lastName: lastName, email: email}}, {new: true}).then(result => result)
}

//UPDATE USER PASSWORD
module.exports.updatePw = async (id, password) => {
	let updatedPw = {
		password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
	}
	return await User.findByIdAndUpdate(id, {$set: updatedPw}, {new: true}).then( result =>{
		if (result){
			result.password = "***"
			return result
		}else{
			return err
		}
	})
}





//CHECK IF EMAIL EXISTS
module.exports.checkEmail = async (reqBody) => {
	const {email} = reqBody

	return await User.findOne({email: email}).then((result, err) =>{
		if(result){
			return true
		} else {
			if(result == null){
				return false
			} else {
				return err
			}
		}
	})
}



// UPDATE isAdmin status to "TRUE" 
module.exports.adminStatusToTrue = async (id) => {
	return await User.findOneAndUpdate({_id: id}, {$set: {isAdmin: true}}).then((result, err) => result ? true : err)
}

// UPDATE isAdmin status to "FALSE" 
module.exports.adminStatusToFalse = async (id) => {
	
	return await User.findOneAndUpdate({_id: id}, {$set: {isAdmin: false}}).then((result, err) => result ? true : err)
}

// DELETE A USER FROM THE DB
module.exports.deleteUser = async (userId) => {
	return await User.findOneAndDelete({_id: userId}).then((result, err) => result ? true : err)
}





