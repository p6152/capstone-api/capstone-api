const mongoose = require(`mongoose`)

const orderSchema = new mongoose.Schema({
		userId: {
			type: String,
			required: [true, `User ID is required`]
		},
		orderedProducts: [
			{
			productId: {
				type: String,
				required: [true, `Product ID is required`]
			},

			price: {
				type: Number,
				required: [true, `Price is required`]
			},
			qty: {
				type: Number,
				required: [true, `Quantity is required`]
			},
			total: {
				type: Number,
				default: 0
			}
			}
		],

		subTotal: {
			type: Number,
			default: 0
		},

		status: {
			type: String,
			default: "Pending"
		},
		
		purchasedOn: {
			type: Date,
			default: new Date()
		},
		dateCompleted: {
			type: Date,
			default: null
		},
}, {timestamps: true})

module.exports = mongoose.model("Order", orderSchema);
