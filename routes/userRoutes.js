const express = require(`express`);
const router = express.Router();

const {
	getAllCustomers,
	register,
	checkEmail,
	login,
	updateInfo,
	updatePw,
	profile,
	adminStatusToTrue,
	adminStatusToFalse,
	deleteUser,
} = require(`./../controllers/userControllers`)

const {verify, verifyAdmin, decode} = require(`./../auth`);
const { json } = require("express/lib/response");

// GET ALL CUSTOMERS
router.get(`/`, async (req, res) => {
	try{
		await getAllCustomers().then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})
router.get('/profile', verify, async (req, res) => {
	const userId = decode(req.headers.authorization).id
	try{
		await profile(userId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


// REGISTER A USER
router.post(`/register`, async (req, res) => {
	try{
		await register(req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//CHECK IF EMAIL ALREADY EXISTS
router.post('/email-exists', async (req, res) => {
	try{
		await checkEmail(req.body).then(result => res.send(result))

	}catch(error){
		res.status(500).json(error)
	}
})


// LOGIN A USER
router.post(`/login`, async (req, res) => {
	try{
		await login(req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//RETRIEVE CUSTOMER INFORMATION
router.get('/:_id', verify, async (req, res) => {
	try{
		await profile(req.params._id).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})




//UPDATE USER INFO
router.patch('/update-user/:_id', verify, async(req, res) => {
	console.log(req.params._id)

	try{
		await updateInfo(req.params._id, req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

//UPDATE USER PASSWORD
router.patch(`/update-password`, verify, async(req, res) => {

	console.log( decode(req.headers.authorization).id )
	console.log(req.body.password)

	const userId = decode(req.headers.authorization).id
	try{
		await updatePw(userId, req.body.password).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

// UPDATE isAdmin status to "TRUE" 
router.patch(`/:id/setAsAdmin`, verifyAdmin, async (req, res) =>{
	// console.log(req.params.id)
	try{
			await adminStatusToTrue(req.params.id).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

// UPDATE isAdmin status to "FALSE"
router.patch(`/:id/setAsCustomer`, verifyAdmin, async (req, res) =>{
	// console.log(req.params.id)
	try{
			await adminStatusToFalse(req.params.id).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

// DELETE USER - Only admin can delete
router.delete(`/:userId/delete-user`, verifyAdmin, async (req, res) =>{

	try{
		await deleteUser(req.params.userId).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}	
})







module.exports = router;


