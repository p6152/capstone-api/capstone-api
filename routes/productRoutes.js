const express = require(`express`)
const router = express.Router()

const {
	addProduct,
	activeProducts,
	showProducts,
	getProduct,
	updateProduct,
	archiveProduct,
	unarchiveProduct,
	deleteProduct,
	getOneProduct
} = require(`./../controllers/productControllers`)

const {verify, verifyAdmin} = require(`./../auth`)

// RETRIEVE ALL ACTIVE PRODUCTS
router.get(`/active-products`, async (req, res) => {

	try{
		await activeProducts().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})	

// CREATE PRODUCT
router.post(`/add-product`, verifyAdmin, async (req, res) => {
	try{
		await addProduct(req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})
//
// RETRIEVE SPECIFIC ACTIVE PRODUCT
router.get(`/active-products/:productId`, verify, async (req, res) =>{
	// console.log(req.params.productId)
	try{
		await getProduct(req.params.productId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

// RETRIEVE SPECIFIC PRODUCT
router.get(`/:productId`, verify, async (req, res) =>{
	// console.log(req.params.productId)
	try{
		await getOneProduct(req.params.productId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})




// RETRIEVE ALL PRODUCTS (ACTIVE AND NOT ACTIVE)
router.get(`/`, verifyAdmin, async (req, res) => {

	try{
		await showProducts().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})	

// UPDATE PRODUCT
router.post(`/update-product/:productId`, verifyAdmin, async (req, res) => {

	try{
		return await updateProduct(req.params.productId, req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//ARCHIVE PRODUCT
router.patch(`/:productId/archive`, verifyAdmin, async (req, res) =>{
	try{
		await archiveProduct(req.params.productId, req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//ARCHIVE PRODUCT
router.patch(`/:productId/unarchive`, verifyAdmin, async (req, res) =>{
	try{
		await unarchiveProduct(req.params.productId, req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

// DELETE PRODUCT
router.delete(`/:productId/delete-product`, verifyAdmin, async (req, res) =>{

	try{
		await deleteProduct(req.params.productId).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}	
})







module.exports = router;