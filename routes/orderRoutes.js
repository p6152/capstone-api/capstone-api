const express = require(`express`);
const router = express.Router();

const {
	createOrder,
	showAllOrders,
	myOrders,
	deleteOrder,
	orderCompleted
} = require(`./../controllers/orderControllers`)

const {verify, verifyAdmin, decode} = require(`./../auth`)





// CREATE AN ORDER - ONLY NON-ADMIN
router.post(`/users/checkout`, verify, async (req, res) => {
	const user = decode(req.headers.authorization).isAdmin
	const data = {
		userId: decode(req.headers.authorization).id,
		productId: req.body.productId,
		price: req.body.price,
		qty: req.body.qty
	}
	if(user == false){
		try{
			await createOrder(data).then(result => res.send(result))
		}catch(err){
			res.status(500).json(err)
		}
	} else{
		res.send(`Only Non-Admin user can order`)
	}
})





// RETRIEVE ALL ORDERS - ONLY ADMIN
router.get(`/users/orders`, verifyAdmin, async (req, res) => {
	try{
		await showAllOrders().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


//RETRIEVE USER"S ORDER/S
router.get(`/users/myOrders`, verify, async (req, res) => {

	const userID = {userId: decode(req.headers.authorization).id}
	const user = decode(req.headers.authorization).isAdmin

	if(user == false){
		try{
			await myOrders(userID).then(result => res.send(result))
		}catch(err){
			res.status(500).json(err)
		}
	} else{
		res.send(`You are not authorized`)
	}

})

// DELETE ORDER
router.delete(`/delete-order`, verifyAdmin, async (req, res) =>{

	try{
		await deleteOrder(req.body).then(result => res.send(`Order has been deleted`))
	}catch(err){
		res.status(500).json(err)
	}	
})


// UPDATE Order status to "Completed" 
router.patch(`/:orderId/complete`, verify, async (req, res) =>{
	try{
		await orderCompleted(req.params.orderId, req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})













module.exports = router;